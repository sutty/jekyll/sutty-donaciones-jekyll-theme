---
title: Donaciones para Sutty
description: 'Sutty es un proyecto de la economía solidaria, hecho con Software Libre y sostenido por un grupo de activistas diverso.'
author:
- Sutty
layout: portada
---

Como iniciativa colectiva y solidaria, nuestro principal motor es poder
desarrollar herramientas que sean de utilidad para diferentes
comunidades de usuaries comprometides con la transformación social.

El proyecto se sostiene desde la reciprocidad. Para que esto pueda
mantenerse, crecer, desarrollarse y  dar respuestas a necesidades o
pedidos concretos de nuestres usuaries y compañeres, hay diferentes
formas en las que pueden colaborar:

* A través de una (o varias) **colaboraciones monetarias**. Muchas de
  las cosas que hacemos requieren del uso de dinero, como pagar
  alquileres, infraestructura, impuestos, trasladarnos en el transporte
  público o cocinarnos una pizza un fin de semana.

  Contactanos por [otros medios](https://sutty.nl/index.html#contacto)
  para hacernos llegar aportes :)

* [Reportando errores](link a cómo reportar errores) o comentarios en el
  uso de Sutty a través de nuestro [formulario de
  contacto](https://sutty.nl/index.html#contacto) o a través de nuestro
  [repositorio en 0xacab](https://0xacab.org/sutty/sutty/-/issues).

* Contribuyendo en el desarrollo de características, plantillas o
  diseños. Contactanos a través del
  [formulario](https://sutty.nl/index.html#contacto).

* Participando en grupos de asistencia mutua (próximamente), canales y/o
  redes sociales orientando a otres usuaries sobre el funcionamiento de
  Sutty.

* Colaborando con el desarrollo de documentación.

* Acercándose a proyectos colectivos en los que Sutty participa, como el
  [Recursero de Salud Trans\*](https://recursero.info/)

* Recomendándonos entre compas y organizaciones para desarrollar
  proyectos y a través de las redes sociales que usen.

* En el panel de administración de tus sitios encontrarás info de
  colaboraciones referidas a plantillas específicas y/o a trabajos que
  hicimos con otres compas con quienes compartimos aportes.

Siempre están invitades a  escribirnos a nuestro [formulario de
contacto](https://sutty.nl/index.html#contacto), con ideas, proyectos,
dudas.
