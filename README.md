# sutty-donaciones-jekyll-theme

A donations page theme.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "sutty-donaciones-jekyll-theme"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: sutty-donaciones-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sutty-donaciones-jekyll-theme

## Usage

Create an article with layout 'portada' and as many posts with layout
'boton' as you want.  The required fields are described under
`_data/layouts`.

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
https://0xacab.org/sutty/jekyll/sutty-donaciones-jekyll-theme. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty's code of
conduct](http://sutty.nl/en/code-of-conduct).

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme,
run `bundle exec jekyll serve` and open your browser at
`http://localhost:4000`. This starts a Jekyll server using your
theme. Add pages, documents, data, etc. like normal to test your theme's
contents. As you make modifications to your theme and to your content,
your site will regenerate and you should see the changes in the browser
after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`,
`_sass` and `assets` tracked with Git will be bundled.

To add a custom directory to your theme-gem, please edit the array in
`sutty-donaciones-jekyll-theme.gemspec` accordingly.

## License

The theme is available as open source under the terms of the [Antifacist
MIT License](LICENSE.txt).

